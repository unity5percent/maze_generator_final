﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class HeightText : MonoBehaviour
{
    public GameObject logoText;

    public void Start()
    {
        logoText.SetActive(false);
    }

    public void OnMouseEnter()
    {
        Debug.Log("Mouse Enter");
        logoText.SetActive(true);
    }

    public void OnMouseExit()
    {
        logoText.SetActive(false);
    }
}
