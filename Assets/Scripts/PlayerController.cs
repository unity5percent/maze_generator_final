﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public float rotationSpeed;

    private Animator anim;
    private Rigidbody rb;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        Vector3 rotationMovement = new Vector3(0, Input.GetAxisRaw("Horizontal"),0 );

        transform.Rotate(rotationMovement * rotationSpeed);


        if (Input.GetAxisRaw("Vertical") > 0)
            rb.velocity = transform.forward * speed;
        

        if (Input.GetAxisRaw("Vertical") < 0)
            rb.velocity = -transform.forward * speed;
        

      
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            anim.SetBool("isWalking", true);
        }
        else

            anim.SetBool("isWalking", false);

    }
}
